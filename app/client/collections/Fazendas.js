Fazendas = new Ground.Collection('fazendas', { connection: null });

FazendasSchema =  new SimpleSchema({

  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  serverId: {
    type: String,
    autoValue: function () {
      if (!this.isInsert)
        this.unset();
    }
  },

  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (!this.isInsert) {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              Info
  //////////////////////////////////////

  name: {
    type: String,
    label: "Nome da Fazenda"
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    optional: true,
  },
});

Fazendas.attachSchema(FazendasSchema);