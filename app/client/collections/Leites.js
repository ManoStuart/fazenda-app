Leites = new Ground.Collection('leites', { connection: null });

LeitesSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    defaultValue: function () {return new Date();},
  },

  ordenha1: {
    type: Number,
    decimal: true,
    min: 0,
    label: "Ordenha da Manhã",
    optional: true,
  },

  ordenha2: {
    type: Number,
    decimal: true,
    min: 0,
    label: "Ordenha da Tarde",
    optional: true,
  },

  ordenha3: {
    type: Number,
    decimal: true,
    min: 0,
    label: "Ordenha da Noite",
    optional: true,
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Leites.attachSchema(LeitesSchema);