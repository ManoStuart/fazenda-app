Inseminacoes = new Ground.Collection('inseminacoes', { connection: null });

InseminacoesSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  reprodutor: {
    type: String,
  },

  inseminador: {
    type: String,
    optional: true,
  },

  condicao: {
    type: String,
    allowedValues: ["Cio Natural", "Prostaglandinas", "Protocolo IATF/TETF"],
    defaultValue: "Cio Natural",
    label: "Condição",
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Inseminacoes.attachSchema(InseminacoesSchema);