Usuarios = new Ground.Collection('usuarios', { connection: null });

UsuariosSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  logged: {
    type: Boolean,
    defaultValue: true,
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  username: {
    type: String,
  },
  
  password: {
    type: String,
    label: "Senha"
  },

  autoSync: {
    type: Boolean,
    label: "Sincronização automática",
    defaultValue: true,
  },

  deleteOnSync: {
    type: Boolean,
    label: "Deletar após sincronização" ,
    defaultValue: false,
  },

  // textSize: {
  //   type: String,
  //   label: "Tamanho da fonte",
  //   defaultValue: "normal",
  //   allowedValues: ['small', 'normal', 'large'],
  // },
});

Usuarios.attachSchema(UsuariosSchema);

registerSchema = new SimpleSchema({
  username: {
    type: String,
  },
  password: {
    type: String,
    label: "Senha",
  },
  confirmation: {
    type: String,
    label: "Confirmar Senha",
    optional: false,
    custom: function(){
      if (this.value !== this.field('password').value) {
          return 'noMatch';
      }
    }
  }
});

registerSchema.messages({
  noMatch: "Senhas diferentes.",
});