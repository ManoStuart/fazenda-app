Outros = new Ground.Collection('outros', { connection: null });

OutrosSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  setor: {
    type: String,
    allowedValues: ['Pecuária de Leite', 'Pecuária de Corte', 'Agricultura', 'Setores de Suporte'],
  },

  processo: {
    type: String,
    allowedValues: ["Produção", "Cria e Recria", "Reprodução", "Alimentação", "Sanidade", "Limpeza e Manutenção", "Administração"],
  },

  evento: {
    type: String,
  },

  number: {
    type: String,
    label: "Número/Categoria do Animal",
  },

  date: {
    type: Date,
    label: "Data",
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Outros.attachSchema(OutrosSchema);