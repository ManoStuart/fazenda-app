Aplicacoes = new Ground.Collection('aplicacoes', { connection: null });

AplicacoesSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  produto: {
    type: String,
  },

  lote: {
    type: String,
    optional: true,
  },

  dose: {
    type: Number,
    decimal: true,
    min: 0,
  },

  evento: {
    type: String,
    allowedValues: ["Nenhum", "Parto", "Secagem", "Desmama"],
    defaultValue: "Nenhum",
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Aplicacoes.attachSchema(AplicacoesSchema);