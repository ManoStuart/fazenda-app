Exames = new Ground.Collection('exames', { connection: null });

ExamesSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  utero: {
    type: String,
    allowedValues: ["Nenhum", "ACD", "ACE", "Aderência", "Cheio de Fluido", "Com Tônos de Estro", "FMAC", "FMOR", "FMUM",
      "Involuído Normal", "Metrite", "PNE", "PRU", "SAD", "SAE", "SAL", "Subinvoluído Normal",
      "TUM", "VAGE", "Outros"],
    label: "Útero",
    defaultValue: "Nenhum",
  },

  ovarioE: {
    type: String,
    allowedValues: ["Nenhum", "Aderência", "ANE", "Cisto", "Cisto Folicular", "Cisto Luteínico", "CL", "CL-", "CL Médio",
    "CL Recente", "CL Tardio", "FIB", "FOL", "Folículo Grande", "Folículo Normal", "Folículo Pequeno", "Fossa de Ovulação",
    "Full Estrus", "OHP", "OSE", "Outros", "Ovário Estático", "Salpinigite", "TUM"],
    label: "Ovário Esquerdo",
    defaultValue: "Nenhum",
  },

  ovarioD: {
    type: String,
    allowedValues: ["Nenhum", "Aderência", "ANE", "Cisto", "Cisto Folicular", "Cisto Luteínico", "CL", "CL-", "CL Médio",
    "CL Recente", "CL Tardio", "FIB", "FOL", "Folículo Grande", "Folículo Normal", "Folículo Pequeno", "Fossa de Ovulação",
    "Full Estrus", "OHP", "OSE", "Outros", "Ovário Estático", "Salpinigite", "TUM"],
    label: "Ovário Direito",
    defaultValue: "Nenhum",
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Exames.attachSchema(ExamesSchema);