Mastites = new Ground.Collection('mastites', { connection: null });

MastitesSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  ordenha: {
    type: String,
    allowedValues: ["Manhã", "Tarde", "Noite", "Não Lactante"],
  },

  tetos: {
    type: Object,
  },

  "tetos.ae": {
    type: String,
    label: "Teto Anterior Esquerdo",
    allowedValues: ["Sem Grumos", "Grumos", "Glândula Inchada", "Sistêmico"],
    defaultValue: "Sem Grumos",
  },

  "tetos.ad": {
    type: String,
    label: "Teto Anterior Direito",
    allowedValues: ["Sem Grumos", "Grumos", "Glândula Inchada", "Sistêmico"],
    defaultValue: "Sem Grumos",
  },

  "tetos.pe": {
    type: String,
    label: "Teto Posterior Esquerdo",
    allowedValues: ["Sem Grumos", "Grumos", "Glândula Inchada", "Sistêmico"],
    defaultValue: "Sem Grumos",
  },

  "tetos.pd": {
    type: String,
    label: "Teto Posterior Direito",
    allowedValues: ["Sem Grumos", "Grumos", "Glândula Inchada", "Sistêmico"],
    defaultValue: "Sem Grumos",
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Mastites.attachSchema(MastitesSchema);