Partos = new Ground.Collection('partos', { connection: null });

PartosSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  tipo: {
    type: String,
    allowedValues: ["Normal", "Aborto", "Auxiliado", "Induzido", "Natimorto", "Prematuro", "Vivo/Natimorto"],
    defaultValue: "Normal",
  },
  grau: {
    type: String,
    optional: true,
    allowedValues: ["Introdução de Mãos", "Bezerro Puxado", "Complicado", "Cezariana"],
  },
  peso: {
    type: Number,
    optional: true,
    decimal: true,
    min: 0,
  },
  ecc: {
    type: Number,
    optional: true,
    decimal: true,
    min: 0,
    max: 5,
  },
  placenta: {
    type: Boolean,
    label: "Retenção de Placenta",
  },
  prostaglandina: {
    type: Boolean,
  },

  crias: {
    type: [Object],
    minCount: 0,
    maxCount: 2,
  },
  "crias.$.number": {
    type: String,
    label: "Número da Cria"
  },
  "crias.$.sexo": {
    type: String,
    allowedValues: ['Macho', 'Fêmea'],
  },
  "crias.$.peso": {
    type: Number,
    decimal: true,
    min: 0,
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Partos.attachSchema(PartosSchema);