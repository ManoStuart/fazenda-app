Secagens = new Ground.Collection('secagens', { connection: null });

SecagensSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  motivo: {
    type: String,
    allowedValues: ["Animal Doente", "Baixa Produção", "Comportamento", "Mastite", "Outros", "Problemas de Casco", "Rotina"],
  },

  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Secagens.attachSchema(SecagensSchema);