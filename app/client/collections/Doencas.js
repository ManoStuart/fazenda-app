Doencas = new Ground.Collection('doencas', { connection: null });

DoencasSchema =  new SimpleSchema({
  
  ////////////////////////////////////// 
  //              Control
  //////////////////////////////////////

  exported: {
    type: Boolean,
    autoValue: function () {
      if (this.isInsert)
        return false;
      else {
        if (!this.isSet)
          return false;
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      } else {
        this.unset();
      }
    }
  },
  ownerId: {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return this.ownerId;
      } else {
        this.unset();
      }
    }
  },
  userId:  {
    type: String,
    autoValue: function() {
      if (this.isInsert) {
        return curUser()._id;
      } else {
        this.unset();
      }
    }
  },

  ////////////////////////////////////// 
  //              INFO
  //////////////////////////////////////

  number: {
    type: String,
    label: "Número do Animal"
  },
  
  date: {
    type: Date,
    label: "Data",
    autoform: {afFieldInput: {type: "datetime-local"}}
  },

  doenca: {
    type: String,
    label: "Doença",
    allowedValues: ['Anaplasmose', 'Artrite', 'Babesiose', 'Botulismo', 'Brucelose', 'Carcinoma Ocular',
      'Deslocamento de abomaso', 'Diarréia', 'Distúrbios Metabólicos', 'Doenças a vírus', 'Doenças Bacterianas',
      'Doenças Parasitárias', 'Empazinamento', 'Figueira', 'Gangrena', 'Insuficiência cardíaca', 'Leucose',
      'Mastite', 'Metrite', 'Neoplasia', 'Outros', 'Pericardite', 'Peritonite', 'Piroplasmose',
      'Pleuropneumonia', 'Pneumonia', 'Pododermatite', 'Raiva', 'Retenção de placenta', 'Rompimento de útero',
      'Septicemia', 'Timpanismo', 'Traumatismo', 'Tristeza parasitária', 'Tuberculose'],
  },
  
  obs: {
    type: String,
    optional: true,
    label: "Observações"
  },
});

Doencas.attachSchema(DoencasSchema);