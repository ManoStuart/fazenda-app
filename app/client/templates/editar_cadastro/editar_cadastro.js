Template.EditarCadastro.helpers({
  getName: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].txt
  },
  getCollection: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].name
  },
  getDoc: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].db.findOne(this.doc);
  }
});

AutoForm.hooks({
  'editForm': {
    before: {
      insert: function(doc) {
        doc.ownerId = Session.get('curFazenda');
        return doc;
      }
    },
    onSuccess: function (operation, result, template) {
      Router.go('viewHistorico', {_id: Session.get('curFazenda'), index: Router.current().params.index});
      IonPopup.show({
        buttons: [{
          text: 'Cadastro Modificado!',
          type: 'button-positive',
          onTap: function() {
            IonPopup.close();
          }
        }]
      });
    },
  }
});