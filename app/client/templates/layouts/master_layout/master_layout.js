Template.MasterLayout.helpers({
});

Template.MasterLayout.events({
  'click .item-cadastro': function (e,t) {
    Router.go("novoCadastro", {
      _id: Session.get('curFazenda'),
      index: e.currentTarget.id
    });
  },

  'click #submit-edit': function (e,t) {
    $("#editForm").submit();
  },
  'click #submit-new': function (e,t) {
    $("#newForm").submit();
  },
});

Template.ionNavBackButton.events({
  'click': function (event, template) {
    if (navigator && navigator.app)
      navigator.app.backHistory()
    else {
      console.log('mais q uai eh esse');
      window.history.back();
    }

    return false;
  }
});