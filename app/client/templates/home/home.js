/*****************************************************************************/
/* Home: Event Handlers */
/*****************************************************************************/
Template.Home.events({
});

Template.MasterLayout.events({
  'click #home-action': function (e,t) {
    IonActionSheet.show({
      titleText: 'Ações',
      buttons: [
        { text: 'Nova Fazenda' },
        { text: 'Sincronizar' },
        { text: 'Preferências' },
      ],
      destructiveText: 'Logout',
      cancelText: 'Cancelar',
      cancel: function() {},
      buttonClicked: function(index) {
        if (index === 0) {
          IonModal.open('NovaFazenda');
        }
        else if (index === 1) {
          SyncFazendas();
        }
        else if (index === 2) {
          IonModal.open('Preferences');
        }
        return true;
      },
      destructiveButtonClicked: function() {
        logoff();
        return true;
      }
    });
  },
});

/*****************************************************************************/
/* Home: Helpers */
/*****************************************************************************/
Template.Home.helpers({
  fazendas: function () {
    var user = curUser();
    if (!user) return;

    return Fazendas.find({userId: user._id});
  },
});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.onCreated(function () {
});

Template.Home.onRendered(function () {
  var user = curUser();
  if (!user || !user.autoSync) return;

  SyncFazendas();
});

Template.Home.onDestroyed(function () {
});

function SyncFazendas () {
  var user = curUser();
  if (!user) return;

  var fazendas = Fazendas.find({userId: user._id}).fetch();

  IonLoading.show({
    customTemplate: '<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Sincronizando fazendas'
  });

  httpAuthed(
    'get', '/api/syncFazendas', {synced: _.pluck(fazendas, "serverId")},
    function (data) {
      _.each(data.new, function (fazenda) {
        fazenda.userId = user._id;
        fazenda.serverId = fazenda._id;
        delete fazenda._id;
        delete fazenda.allowedWrite;

        Fazendas.insert(fazenda);
      });

      IonLoading.hide();
    }
  );
}