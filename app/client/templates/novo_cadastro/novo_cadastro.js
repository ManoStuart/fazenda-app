Template.NovoCadastro.helpers({
  getName: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].txt
  },
  getCollection: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].name
  },
  returnPath: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].back;
  },
});

AutoForm.hooks({
  'newForm': {
    before: {
      insert: function(doc) {
        doc.ownerId = Session.get('curFazenda');
        return doc;
      }
    },
    onSuccess: function (operation, result, template) {
      IonPopup.show({
        buttons: [{
          text: 'Cadastro Criado!',
          type: 'button-positive',
          onTap: function() {
            IonPopup.close();
          }
        }]
      });
    },
  }
});