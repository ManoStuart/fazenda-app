AutoForm.hooks({
  'newUser': {
    onSubmit: function (insertDoc, updateDoc, currentDoc) {
      var user = Usuarios.findOne({username: insertDoc.username});

      if (user) {
        IonPopup.alert({
          title: 'Username ja cadastrado.',
          cssClass: "assertive",
        });
      } else {
        delete insertDoc.confirmation;
        IonLoading.show();

        callPost(
          "/api/users",
          insertDoc,
          {},
          function(data) {
            insertDoc._id = data.data._id;

            Usuarios.insert(insertDoc, function (e,r) {
              IonLoading.hide();
              Router.go('home', {_id: r});
            });
          },
          {403: 'Username ja cadastrado.'}
        );
      }

      this.done();
      return false;
    },
  }
});

AutoForm.hooks({
  'loginUser': {
    onSubmit: function (insertDoc, updateDoc, currentDoc) {
      var user = Usuarios.findOne({username: insertDoc.username, password: insertDoc.password});

      if (user) {
        Usuarios.update({_id: user._id}, {$set: {logged: true}});
        Router.go('home');
        this.done();
      } else {
        IonLoading.show();

        logginRemote(
          insertDoc,
          function (data) {
            console.log("mais q auai");
            console.log(data);
            insertDoc._id = data.data.userId;
            Usuarios.insert(insertDoc, function (e,r) {
              Router.go('home', {_id: r});
              IonLoading.hide();
              logoffRemote();
            });
          }
        );
      }

      this.done();
      return false;
    },
  }
});

Template.Login.events({
  'click #toggleLogin': function (e,t) {
    Session.set('isLogin', !Session.get('isLogin'));
  },
});

Template.Login.helpers({
  isLogin: function () {
    return Session.get('isLogin');
  },
});

Template.Login.onCreated(function () {
  logoff();

  Session.set('isLogin', true);
});

Template.Login.onDestroyed(function () {
  delete Session.keys['isLogin'];
});