Template.ViewHistorico.events({
  'click .log-item': function (e,t) {
    IonActionSheet.show({
      titleText: 'Ações',
      buttons: [
        { text: 'Editar Cadastro' },
      ],
      destructiveText: 'Deletar Cadastro',
      cancelText: 'Cancelar',
      cancel: function() {},
      buttonClicked: function(index) {
        if (index === 0) {
          Router.go("editarCadastro", {
            _id: Session.get('curFazenda'),
            doc: e.currentTarget.id,
            index: t.data.index,
          });
        }
        return true;
      },
      destructiveButtonClicked: function() {
        IonPopup.confirm({
          title: 'Deletar Cadastro?',
          onOk: function() {
            var obj = logCollections[t.data.index];
            obj.db.remove(e.currentTarget.id);
          },
          onCancel: function() {}
        });
        return true;
      }
    });
  },
});

Template.MasterLayout.events({
  'click .item-hist': function (e,t) {
    Router.go("viewHistorico", {
      _id: Session.get('curFazenda'),
      index: e.currentTarget.id
    });
  },
  'click #hist-action': function (e,t) {
    var obj = logCollections[t.data.index];

    IonActionSheet.show({
      titleText: 'Ações',
      buttons: [
        { text: 'Exportar ' + obj.txt},
        { text: 'Filtros' },
      ],
      destructiveText: 'Deletar Cadastros Selecionados',
      cancelText: 'Cancelar',
      cancel: function() {},
      buttonClicked: function(index) {
        if (index === 0) {
          if (Session.get('query').exported !== false)
            IonPopup.confirm({
              title: 'Alguns cadastros serão re-exportados.',
              onOk: function() {
                ExportCsv(t.data.index);
              },
              onCancel: function() {}
            });
          else
            ExportCsv(t.data.index);
        }
        else if (index === 1) {
          IonModal.open("HistFilters");
        }
        return true;
      },
      destructiveButtonClicked: function() {
        IonPopup.confirm({
          title: 'Deletar Cadastros Selecionados?',
          onOk: function() {
            DeleteAll(obj.db, Session.get('query'));
          },
          onCancel: function() {}
        });
        return true;
      }
    });
  },
});

Template.ViewHistorico.helpers({
  logs: function (collection) {
    return logCollections[this.index].db.find(Session.get('query'));
  },
  getName: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return logCollections[this.index].txt
  },
  returnPath: function () {
    if (!logCollections || !logCollections[this.index]) return;
    return "hist" + logCollections[this.index].back;
  },
  getExported: function (exported) {
    return exported ? "Exportado" : "Não Exportado";
  },
  getColor: function (exported) {
    return exported ? "balanced" : "assertive";
  },
});

Template.ViewHistorico.onCreated(function () {
  Session.set('query', {ownerId: Session.get('curFazenda'), userId: curUser()._id});
});

Template.ViewHistorico.onDestroyed(function () {
  delete Session.keys['query'];
});