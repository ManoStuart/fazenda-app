/*****************************************************************************/
/* Fazenda: Event Handlers */
/*****************************************************************************/
Template.Fazenda.events({
});

Template.MasterLayout.events({
  'click #fazenda-action': function (e,t) {
    IonActionSheet.show({
      titleText: 'Ações',
      buttons: [
        { text: 'Exportar Cadastros' },
        { text: 'Editar Fazenda' },
        { text: 'Deletar Cadastros Exportados' },
      ],
      destructiveText: 'Deletar Fazenda',
      cancelText: 'Cancelar',
      cancel: function() {},
      buttonClicked: function(index) {
        if (index === 0) {
          ExportCsv("Todos");
        }
        else if (index === 1) {
          IonModal.open('EditarFazenda');
        }
        else if (index === 2) {
          IonPopup.confirm({
            title: 'Deletar Cadastros Exportados?',
            onOk: function() {
              DeleteAllExported(Session.get('curFazenda'));
            },
            onCancel: function() {}
          });
        }
        return true;
      },
      destructiveButtonClicked: function() {
        IonPopup.confirm({
          title: 'Deletar Fazenda?',
          onOk: function() {
            PurgeAll(Session.get('curFazenda'));
            Router.go("home");
          },
          onCancel: function() {}
        });
        return true;
      }
    });
  },
});

/*****************************************************************************/
/* Fazenda: Helpers */
/*****************************************************************************/
Template.Fazenda.helpers({
  NomeFazenda: function () {
    var fazenda = Fazendas.findOne(this._id);
    if (fazenda) return fazenda.name;
    return '';
  }
});

/*****************************************************************************/
/* Fazenda: Lifecycle Hooks */
/*****************************************************************************/
Template.Fazenda.onCreated(function () {
  Session.set('query', {ownerId: Session.get('curFazenda'), userId: curUser()._id, exported: false});
});

Template.Fazenda.onRendered(function () {
});

Template.Fazenda.onDestroyed(function () {
  delete Session.keys['query'];
});
