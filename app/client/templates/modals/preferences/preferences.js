AutoForm.hooks({
  'preferences-form': {
    onSuccess: function (operation, result, template) {
      IonModal.close();
    },
  }
});

Template.Preferences.helpers({
  getDoc: function () {
    return curUser();
  },
});