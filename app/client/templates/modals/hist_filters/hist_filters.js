histFiltersSchema = new SimpleSchema({
  exported: {
    type: String,
    allowedValues: ['Exportado', 'Não exportado', 'Ambos'],
    label: 'Situação do Cadastro',
    defaultValue: 'Ambos',
  },
  
  start: {
    type: Date,
    optional: true,
    label: 'Criado depois de',
  },

  end: {
    type: Date,
    optional: true,
    label: 'Criado ate',
  },

  obs: {
    type: String,
    label: 'Possui observações',
    allowedValues: ['Sim', 'Não'],
    optional: true,
  },
});

Template.HistFilters.helpers({
  histFiltersSchema: function() {
    return histFiltersSchema;
  }
});

AutoForm.hooks({
  histFilters: {
    onSubmit: function (doc) {
      var query = {ownerId: Session.get('curFazenda')};

      if (doc.exported !== 'Ambos' && doc.exported !== undefined) {
        query.exported = doc.exported === 'Exportado' ? true : false;
      }

      if (doc.obs) {
        query.obs = doc.obs === 'Sim' ? {$exists: true} : {$exists: false};
      }

      if (doc.start) {
        query.date = {$gte: doc.start};
      }

      if (doc.end) {
        if (!query.date) query.date = {};
        query.date.$lte = doc.end;
      }

      Session.set('query', query);

      IonModal.close();
      this.done();
      return false;
    }
  }
});