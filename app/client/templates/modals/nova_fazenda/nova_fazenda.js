AutoForm.hooks({
  'fazendas-form': {
    before: {
      insert: function(doc) {
        var self = this;
        IonLoading.show();

        httpAuthed(
          'post', '/api/fazendas', doc,
          function (data) {
            doc.serverId = data._id;
            doc.userId = curUser()._id;
            self.result(doc);
          }
        );
      }
    },
    after: {
      insert: function(error, result) {
        IonLoading.hide();
        if (error) return console.log(error);

        IonModal.close();
        Router.go('fazenda', {_id: result});
      }
    },
  }
});

AutoForm.hooks({
  'fazendas-edit': {
    onSuccess: function (operation, result, template) {
      IonModal.close();
    },
  }
});

Template.EditarFazenda.helpers({
  getDoc: function () {
    return Fazendas.findOne(Session.get('curFazenda'));
  },
});