serverUrl = "http://192.168.1.207:3000";
// serverUrl = "http://ec2-54-152-208-189.compute-1.amazonaws.com";

callPost = function (path, data, auth, callback, error) {
  callHttp('post', path, data, auth, callback, error);
}

callGet = function (path, data, auth, callback, error) {
  callHttp('get', path, data, auth, callback, error);
}

callHttp = function (method, path, data = {}, auth, callback = _.noop, error) {
  if (!auth) auth = Session.get('auth') || {};

  if (Meteor.isCordova) {
    if (!cordovaHTTP[method]) return;

    cordovaHTTP[method](
      serverUrl + path,
      data,
      auth,
      function (response) {
        if (callback) callback(JSON.parse(response.data));
      },
      function (response) {
        httpErrorHandler(response, error || {});
        IonLoading.hide();
      }
    );
  } else {
    // console.log('mais q uai eh esse');
    // console.log(path, data);
    // Meteor.call('callPost', path, data, callback);
  }
}

var online = true;

document.addEventListener("offline", function () {
  online = false;
}, false);

document.addEventListener("online", function () {
  online = true;
}, false);


httpErrorHandler = function (response, messages) {
  // ultraDebug(response);

  if (!online) {
    return IonPopup.alert({
      title: "Verifique sua conexão de internet.",
      cssClass: "assertive",
    });
  }

  _.defaults(messages, {
    401: "Acesso negado.",
    404: "Não encontrado.",
    403: "Acesso negado.",
    408: "Verifique sua conexão de internet.",
    // 500: "Erro no servidor.",
  });

  var status = response.status;
  var message = response.error.split('\n')[0];
  var match = message.match(/\[([0-9]{3})\]/);
  if (match) status = Number(match[1]);

  IonPopup.alert({
    title: messages[status] || message || "Erro [" + status + ']!',
    cssClass: "assertive",
  });
}