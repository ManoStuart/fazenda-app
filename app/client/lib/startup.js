logCollections = [];

Meteor.startup(function () {
  AutoForm.setDefaultTemplate('ionic');
  SimpleSchema.messages({required: "[label] é necessário."});

  logCollections = [
    {db: Doencas, txt: "Doenças", index: 0, name: 'Doencas', back: "sanidade"},
    {db: Mastites, txt: "Mastites", index: 1, name: 'Mastites', back: "sanidade"},
    {db: Aplicacoes, txt: "Aplicações", index: 2, name: 'Aplicacoes', back: "sanidade"},
    {db: Diagnosticos, txt: "Diagnósticos", index: 3, name: 'Diagnosticos', back: "reproducao"},
    {db: Exames, txt: "Exames", index: 4, name: 'Exames', back: "reproducao"},
    {db: Partos, txt: "Partos", index: 5, name: 'Partos', back: "reproducao"},
    {db: Inseminacoes, txt: "Inseminações", index: 6, name: 'Inseminacoes', back: "reproducao"},
    {db: Leites, txt: "Pesagens de Leite", index: 7, name: 'Leites', back: "producao"},
    {db: Pesagens, txt: "Pesagens de Gado", index: 8, name: 'Pesagens', back: "producao"},
    {db: Secagens, txt: "Secagens", index: 9, name: 'Secagens', back: "producao"},
    {db: Outros, txt: "Fazenda", index: 10, name: 'Outros', back: "fazenda"},
  ]
});

Meteor.disconnect();

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady(){
  document.addEventListener("backbutton", function(e) {
    var current = Router.current().route.getName();
    e.preventDefault();

    if(current === "login" || current === "home") {
      IonPopup.confirm({
        title: 'Sair?',
        onOk: function() {
          navigator.app.exitApp();
        },
        onCancel: function() {}
      });
    }
    else {
      navigator.app.backHistory()
    }
  }, false);
}