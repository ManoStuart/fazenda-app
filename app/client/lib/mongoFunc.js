DeleteAll = function (collection, query) {
  var ids = collection.find(query, {fields: {_id: 1}});

  ids.forEach(function (id) {
    collection.remove(id);
  });
}

DeleteAllExported = function (id) {
  logCollections.forEach(function (obj) {
    DeleteAll(obj.db, {ownerId: id, exported: true});
  });
}

PurgeAll = function (id) {
  logCollections.forEach(function (obj) {
    DeleteAll(obj.db, {ownerId: id});
  });

  Fazendas.remove(id);
}

UpdateAll = function (collection, query, set) {
  var ids = collection.find(query, {fields: {_id: 1}});

  ids.forEach(function (id) {
    collection.update(id, set);
  });
}