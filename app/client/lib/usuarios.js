logoff = function () {
  var users = Usuarios.find({logged: true});
  users.forEach(function (user) {
    Usuarios.update({_id: user._id}, {$set: {logged: false}});
  });

  Router.go('login');
}

curUser = function () {
  return Usuarios.findOne({logged: true});
}

logginRemote = function (data, callback) {
  if (!data) data = curUser();

  callPost(
    "/api/login",
    data,
    {},
    function (data) {
      storeAuth(data);
      if (callback) callback(data);
    },
    {401: 'Senha ou Username errados!'}
  );
}

function storeAuth (data) {
  Session.set('auth', {"X-User-Id": data.data.userId, "X-Auth-Token": data.data.authToken});
}

logoffRemote = function (callback) {
  var auth = Session.get('auth');
  if (!auth) return;

  callPost(
    "/api/logout",
    {},
    auth,
    function (data) {
      Session.set('auth', {});
      if (callback) callback(data);
    }
  );
}

httpAuthed = function (method, path, params, callback, error) {
  logginRemote(
    curUser(),
    function (data) {
      callHttp(
        method,
        path,
        params,
        Session.get('auth'),
        function (data) {
          if (callback) callback(data);
          logoffRemote();
        },
        error
      );
    }
  );
}