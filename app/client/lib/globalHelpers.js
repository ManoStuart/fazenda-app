var helpers = {
  prettifyDate: function (date) {
    return moment(date).format('L LT');
  },
};

_.each(helpers, function (helper, key) {
  Handlebars.registerHelper(key, helper);
});


ultraDebug = function (obj, name) {
  console.log("debugging: " + name);
  for (var key in obj) {
    console.log(key + ": ");
    console.log(obj[key]);
  }
}