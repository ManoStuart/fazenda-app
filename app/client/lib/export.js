ExportCsv = function (type, callback) {
  var user = curUser();
  var fazenda = Fazendas.findOne({_id: Session.get('curFazenda')});
  if (!user || !fazenda) return;

  var newData = {};
  var query = Session.get('query');

  var length = 0;

  // prepare data to be exported
  _.each(logCollections, function (collection, index) {
    if (type !== 'Todos' && type != index) return;

    var name = collection.name.toLowerCase();
    newData[name] = collection.db.find(query).fetch();

    if (newData[name].length == 0) delete newData[name];
    else {
      _.each(newData[name], function (log) {
        log.ownerId = fazenda.serverId;
      });

      length += newData[name].length;
    }
  });

  if (length == 0) {
    IonPopup.alert({
      title: 'Nenhum cadastro para ser exportado.',
      cssClass: "assertive",
    });

    return;
  }

  IonLoading.show({
    customTemplate: '<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Exportando ' + length + " cadastros..."
  });

  // Make http call
  httpAuthed(
    'post', '/api/importAll/' + fazenda.serverId,
    newData,
    function (data) {
      updateExported(data.new, user.deleteOnSync, callback);
    }
  );
}



updateExported = function (exported, shouldDelete, callback) {
  var length = 0;

  _.each(logCollections, function (collection) {
    var name = collection.name.toLowerCase();
    if (!exported[name] || exported[name].length == 0) return;

    _.each(exported[name], function (log) {
      if (shouldDelete)
        collection.db.remove({_id: log}, callback); 
      else
        collection.db.update({_id: log}, {$set: {exported: true}}, callback);
    });

    length += exported[name].length;
  });

  IonLoading.hide();
  IonPopup.alert({
    title: length + ' cadastros exportados com sucesso.',
    cssClass: "assertive",
  });
}