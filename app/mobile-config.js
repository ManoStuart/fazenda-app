App.info({
  id: 'com.example.matt.uber',
  name: 'Minha Fazenda',
  description: 'App integration for Ideagri software.',
  author: 'ManoStuart',
  email: 'arthhur.ribeiro@gmail.com',
  version: '0.0.1'
});

App.icons({
  // 'android_ldpi': 'resources/icons/logo4.png',
  'android_mdpi': 'resources/icons/logo3.png',
  'android_hdpi': 'resources/icons/logo2.png',
  'android_xhdpi': 'resources/icons/logo1.png'
});


App.launchScreens({
  "android_mdpi_portrait": 'resources/icons/logo1.png',
  "android_hdpi_portrait": 'resources/icons/logo1.png',
  "android_xhdpi_portrait": 'resources/icons/logo1.png',
  "android_xxhdpi_portrait": 'resources/icons/logo1.png',
});

App.setPreference('Orientation', 'portrait');
App.accessRule('*');
