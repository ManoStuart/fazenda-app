DefaultController = RouteController.extend({
  waitOn: function () {
  },

  data: function () {
    Session.set("curFazenda", this.params._id);
    return this.params;
  },

  action: function () {
    this.render();
  }
});