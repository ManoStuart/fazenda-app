Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});


Router.route('/', {name: 'home', controller: 'DefaultController', where: 'client'});
Router.route('/login', {name: 'login', controller: 'DefaultController', where: 'client'});
Router.route('/fazenda/:_id', {name: 'fazenda', controller: 'DefaultController', where: 'client'});

Router.route('/sanidade/:_id', {name: 'sanidade', controller: 'DefaultController', where: 'client'});
Router.route('/reproducao/:_id', {name: 'reproducao', controller: 'DefaultController', where: 'client'});
Router.route('/producao/:_id', {name: 'producao', controller: 'DefaultController', where: 'client'});

Router.route('/editar/:_id/:doc/:index', {name: 'editarCadastro', controller: 'DefaultController', where: 'client'});
Router.route('/novo/:_id/:index', {name: 'novoCadastro', controller: 'DefaultController', where: 'client'});

Router.route('/geral/:_id', {name: 'geral', controller: 'DefaultController', where: 'client'});

Router.route('/historico/:_id', {name: 'historico', controller: 'DefaultController', where: 'client'});
Router.route('/historico/sanidade/:_id', {name: 'histsanidade', controller: 'DefaultController', where: 'client'});
Router.route('/historico/reproducao/:_id', {name: 'histreproducao', controller: 'DefaultController', where: 'client'});
Router.route('/historico/producao/:_id', {name: 'histproducao', controller: 'DefaultController', where: 'client'});
Router.route('/historico/show/:_id/:index', {name: 'viewHistorico', controller: 'DefaultController', where: 'client'});


var onBeforeActions = {
  loginRequired: function (pause) {
    if (!curUser()) {
      Router.go('login');
      this.next();
      return pause;
    } else {
      this.next();
    }
  }
};

Router.onBeforeAction(onBeforeActions.loginRequired,{except: ['login']});